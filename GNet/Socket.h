#include <string>
#include "GNet.h"

class SocketException
{
	std::string message;
public:
	SocketException(const std::string& message, bool inclSysMsg = false) noexcept;
	~SocketException() noexcept;
	const char* what() const noexcept;
};

class UDPSocket
{
	int sockDesc;
public:
	UDPSocket();
	UDPSocket(unsigned short localPort);
	UDPSocket(const std::string& localAddress, unsigned short localPort);
	~UDPSocket();
	std::string getLocalAddress();
	unsigned short getLocalPort();
	void disconnect();
	void sendTo(const void* buffer, int bufferLen, const std::string& foreignAddress, unsigned short foreignPort);
	int recvFrom(void* buffer, int bufferLen, std::string& sourceAddress, unsigned short& sourcePort);
	//void setMulticastTTL(unsigned char multicastTTL);
	//void joinGroup(const std::string& multicastGroup);
	//void leaveGroup(const std::string& multicastGroup);
private:
	void setBroadcast();
};