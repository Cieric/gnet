#include "pch.h"
#include "Socket.h"
#include "Defines.h"

SocketException::SocketException(const std::string& message, bool inclSysMsg) noexcept :
	message(message)
{
	if (inclSysMsg) {
		this->message.append(": ");
		char buffer[1024];
		strerror_s(buffer, errno);
		buffer[1023] = '\0';
		this->message.append(buffer);
	}
}

SocketException::~SocketException() noexcept {
}

const char* SocketException::what() const throw() {
	return message.c_str();
}

UDPSocket::UDPSocket() {
	if ((sockDesc = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0) {
		throw SocketException("Socket creation failed (socket())", true);
	}
	setBroadcast();
}

UDPSocket::UDPSocket(unsigned short localPort)
{
	if ((sockDesc = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0) {
		throw SocketException("Socket creation failed (socket())", true);
	}
	// Bind the socket to its port
	sockaddr_in localAddr;
	memset(&localAddr, 0, sizeof(localAddr));
	localAddr.sin_family = AF_INET;
	localAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	localAddr.sin_port = htons(localPort);

	if (bind(sockDesc, (sockaddr*)& localAddr, sizeof(sockaddr_in)) < 0) {
		throw SocketException("Set of local port failed (bind())", true);
	}
	setBroadcast();
}

static void fillAddr(const std::string& address, unsigned short port, sockaddr_in& addr) {
	memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;

	//hostent* host;
	std::string sPort = std::to_string(port);
	//if ((host = gethostbyname(address.c_str())) == NULL) {
	//	throw SocketException("Failed to resolve name (gethostbyname())");
	//}

	struct addrinfo hints = { 0 }, * addrs;
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_UDP;

	const int status = getaddrinfo(address.c_str(), sPort.c_str(), &hints, &addrs);
	if (status != 0)
	{
		//fprintf(stderr, "%s: %s\n", hostname, gai_strerror(status));
		exit(EXIT_FAILURE);
	}

	addr = *(sockaddr_in*)addrs->ai_addr;

	//addr.sin_addr.s_addr = *((unsigned long*)host->h_addr_list[0]);
	//addr.sin_port = htons(port);
}

UDPSocket::UDPSocket(const std::string& localAddress, unsigned short localPort)
{
	if ((sockDesc = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0) {
		throw SocketException("Socket creation failed (socket())", true);
	}
	
	sockaddr_in localAddr;
	inet_pton(AF_INET, localAddress.c_str(), &(localAddr.sin_addr));

	if (bind(sockDesc, (sockaddr*)& localAddr, sizeof(sockaddr_in)) < 0) {
		throw SocketException("Set of local address and port failed (bind())", true);
	}

	setBroadcast();
}

UDPSocket::~UDPSocket()
{
	::closesocket(sockDesc);
}

std::string UDPSocket::getLocalAddress() {
	sockaddr_in addr;
	unsigned int addr_len = sizeof(addr);

	if (getsockname(sockDesc, (sockaddr*)& addr, (int*)& addr_len) < 0) {
		throw SocketException("Fetch of local address failed (getsockname())", true);
	}
	char temp[INET6_ADDRSTRLEN];
	inet_ntop(addr.sin_family, &addr, temp, INET6_ADDRSTRLEN);
	return std::string(temp);
}

unsigned short UDPSocket::getLocalPort() {
	sockaddr_in addr;
	unsigned int addr_len = sizeof(addr);

	if (getsockname(sockDesc, (sockaddr*)& addr, (int*)& addr_len) < 0) {
		throw SocketException("Fetch of local port failed (getsockname())", true);
	}
	return ntohs(addr.sin_port);
}

void UDPSocket::disconnect()
{
	sockaddr_in nullAddr;
	memset(&nullAddr, 0, sizeof(nullAddr));
	nullAddr.sin_family = AF_UNSPEC;

	// Try to disconnect
	if (::connect(sockDesc, (sockaddr*)& nullAddr, sizeof(nullAddr)) < 0) {
		if (errno != GNETNOSUPPORT) {
			throw SocketException("Disconnect failed (connect())", true);
		}
	}
}

void UDPSocket::sendTo(const void* buffer, int bufferLen, const std::string& foreignAddress, unsigned short foreignPort)
{
	sockaddr_in destAddr;
	fillAddr(foreignAddress, foreignPort, destAddr);

	int res = sendto(sockDesc, (char*)buffer, bufferLen, 0, (sockaddr*)& destAddr, sizeof(destAddr));
	if (res != bufferLen) {
		throw SocketException("Send failed (sendto())", true);
	}
}

int UDPSocket::recvFrom(void* buffer, int bufferLen, std::string& sourceAddress, unsigned short& sourcePort)
{
	sockaddr_in clntAddr;
	socklen_t addrLen = sizeof(clntAddr);
	int rtn;
	if ((rtn = recvfrom(sockDesc, (char*)buffer, bufferLen, 0, (sockaddr*)& clntAddr, (socklen_t*)& addrLen)) < 0) {
		throw SocketException("Receive failed (recvfrom())", true);
	}

	char temp[INET6_ADDRSTRLEN];
	inet_ntop(clntAddr.sin_family, &clntAddr, temp, INET6_ADDRSTRLEN);
	sourceAddress = std::string(temp);
	sourcePort = ntohs(clntAddr.sin_port);

	return rtn;
}

void UDPSocket::setBroadcast()
{
	int broadcastPermission = 1;
	setsockopt(sockDesc, SOL_SOCKET, SO_BROADCAST, (char*)& broadcastPermission, sizeof(broadcastPermission));
}
