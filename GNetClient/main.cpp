//#include <GNet.h>
#include <Socket.h>

int main(int argc, char** argv)
{
	unsigned short bufferSize = 65507;
	char* buffer = (char*)malloc(bufferSize);

	if (buffer == nullptr)
		return 1;

	strcpy_s(buffer, bufferSize, "Hello World!");
	UDPSocket sock = UDPSocket(9909);

	sock.sendTo(buffer, strlen(buffer)+1, "127.0.0.1", 9909);
	
	std::string foreignAddress;
	unsigned short port;
	sock.recvFrom(buffer, bufferSize, foreignAddress, port);

	printf("response: %s\n", buffer);

	return 0;
}